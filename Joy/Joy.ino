// Arduino pin numbers
#include <Adafruit_NeoPixel.h>
#define PIN5    A0 //Pin des LEDs
#define LED_COUNT5 30//nb de LEDs
const int SW_pin = A5; // digital pin connected to switch output
int A = 0 ;
int sc = 0 ;

Adafruit_NeoPixel strip5 = Adafruit_NeoPixel (LED_COUNT5, PIN5, NEO_GRB + NEO_KHZ800);

void setup() {
  pinMode(SW_pin, INPUT);
  digitalWrite(SW_pin, HIGH);
  strip5.begin();
  strip5.show();
}

void loop() {
  if(digitalRead(SW_pin) == 0){
    sc++;
    while(digitalRead(SW_pin) == 0){}
    }
  else if(sc == 1){
    charg5(50, 0, 0, 100);
  }
  else if(sc == 2){
    strob(200, 0, 0, 100);
  }else{
      for (int i = 0; i < LED_COUNT5; i++){
      strip5.setPixelColor(i, 0, 100, 0);
      strip5.show();
      }
    }
  
  delay(500);
}

void bande5(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT5; i++){
    strip5.setPixelColor(i, R, G, B);
  }
}

void charg5(int t, int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT5; i++){
    strip5.setPixelColor(i, R, G, B);
    strip5.show();
    delay(t);
  }
}

void strob(int t, int R, int G, int B) { // (durée pause, valeur Red, valeur Green, valeur Blue)
  bande5(R, G, B);
  strip5.show();
  delay(t);
  bande5(0, 0, 0);
  strip5.show();
  delay(t);
}

void puls(int t, int m){ // (vitess lent 1 < rapide 50, max green)
  for (int i = 0; i < m; i = i+t){
    bande5(0, 0, 250);
    strip5.show(); 
  }
  for (int i = m; i > 0; i = i-t){
    bande5(0, 0, 250);
    strip5.show();    
  }
} 
