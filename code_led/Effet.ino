void bande(int R, int G, int B) { // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT; i++){
    strip.setPixelColor(i, R, G, B);
  }
}
void bande1(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT1; i++){
    strip1.setPixelColor(i, R, G, B);
  }
}

void bande2(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT2; i++){
    strip2.setPixelColor(i, R, G, B);
  }
}

void bande3(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT3; i++){
    strip3.setPixelColor(i, R, G, B);
  }
}

void bande4(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT4; i++){
    strip4.setPixelColor(i, R, G, B);
  }
}

void bande5(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT5; i++){
    strip5.setPixelColor(i, R, G, B);
  }
}

void charg(int R, int G, int B) { // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT; i++){
    strip.setPixelColor(i, R, G, B);
    strip.show();
  }
}
void charg1(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT1; i++){
    strip1.setPixelColor(i, R, G, B);
    strip1.show();
  }
}

void charg2(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT2; i++){
    strip2.setPixelColor(i, R, G, B);
    strip2.show();
  }
}

void charg3(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT3; i++){
    strip3.setPixelColor(i, R, G, B);
    strip3.show();
  }
}

void charg4(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT4; i++){
    strip4.setPixelColor(i, R, G, B);
    strip4.show();
  }
}

void charg5(int R, int G, int B){ // (valeur Red, valeur Green, valeur Blue)
  for (int i = 0; i < LED_COUNT5; i++){
    strip5.setPixelColor(i, R, G, B);
    strip5.show();
  }
}

void showall(){
  strip.show();
  strip1.show();
  strip2.show();
  strip3.show();
  strip4.show();
  strip5.show();
}
void all(int R, int G, int B) { // (valeur Red, valeur Green, valeur Blue)
  bande(R, G, B);
  bande1(R, G, B);
  bande2(R, G, B); 
  bande3(R, G, B);
  bande4(R, G, B);
  bande5(R, G, B);
  showall();
}

void chargall(int R, int G, int B) { // (valeur Red, valeur Green, valeur Blue)
  charg(R, G, B);
  charg2(R, G, B);
  charg4(R, G, B);
  charg5(R, G, B);
  charg3(R, G, B);
  charg1(R, G, B);
}


void strob(int t, int R, int G, int B) { // (durée pause, valeur Red, valeur Green, valeur Blue)
  all(R, G, B);
  delay(t);
  all(0, 0, 0);
  delay(t);
}

void puls(int t, int m){ // (vitess lent 1 < rapide 50, max green)
  for (int i = 0; i < m; i = i+t){
    all(0, i, 250);
    showall();    
  }
  for (int i = m; i > 0; i = i-t){
    all(0, i, 250);
    showall();    
  }
} 
